﻿import React, { useState, createContext } from 'react';

export const PostMsgContext = createContext();

export const PostMsgProvider = props => {
    const [posts, setPosts] = useState([
        {
            "id": "8658c1d0-9eda-4a90-95e1-8001e8eb6036",
            "titolo": "La casa",
            "like": 5,
            "num_msg": 2,
            "commenti": [
                {
                    "id": "13g5354f",
                    "testo": "Ciao, sono molto interessato alla sua offerta"
                },
                {
                    "id": "2f435y63",
                    "testo": "Ciao, io le sono pronto a sborsare 15.000€"
                }
            ]
        },
        {
            "id": "f9005b4e-975e-433d-a646-79df172e1dbb",
            "titolo": "Finestra in vetro",
            "like": 7,
            "num_msg": 2,
            "commenti": [
                {
                    "id": "1fg35hg3",
                    "testo": "Ciao, che offerta avete"
                },
                {
                    "id": "2g53h335f",
                    "testo": "Proprio quello che mi serviva amico, grande!!!"
                }
            ]
        }
    ]);

    return (
        <PostMsgContext.Provider value={[posts, setPosts]}>
            {props.children}
        </PostMsgContext.Provider>
    );
}