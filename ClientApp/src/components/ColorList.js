﻿import React, { Component } from 'react';
import { StellaRating } from './StellaRating';

const Colore = ({ titolo, colore, voto = 0, rimuoviColore = f => f, votaColore=f=>f }) =>
    <div className="col-md-6 form-group">
        <section className="section-color ">
            <div className="form-inline">
                <h1>{titolo}</h1>
                <div className="pull-right">
                    <button onClick={rimuoviColore}>X</button>
                </div>
            </div>

            <div className="color"
                style={{ backgroundColor: colore }}>
            </div>
            <div className="rating">
                <StellaRating stelleSelezionate={voto} votaColore={votaColore} />
            </div>
        </section>
    </div>


export const ColorList = ({ colori = [], votaColore = f => f, rimuoviColore = f => f }) =>
    <div className="color-list row ">
        {(colori.length === 0) ?
            <p>Non ci sono colori. (aggiungi un colore)</p> :
            colori.map(colore => <Colore key={colore.id} {...colore} votaColore={(voto) => votaColore(colore.id, voto)} rimuoviColore={() => rimuoviColore(colore.id)}/>)
        }
    </div>