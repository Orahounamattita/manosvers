﻿import React, { Component, useState, useContext } from 'react';
import { PostMsgContext } from '../context/PostMsgContext';

const Comment = ({ testo }) => {
    return (
        <div className="col-md-10 form-group msg-container">
            <div>
                {testo}
            </div>
        </div>
    );
}


export const ListComment = ({ id}) => {

    const [posts, setPosts] = useContext(PostMsgContext);
    const postSelected = posts.filter(post => (post.id === id));
    return (
        <div className="comment-list row ">
            {(postSelected[0].commenti.length === 0) ?
                <p>Non ci sono commenti. (aggiungi un commento)</p> :
                postSelected[0].commenti.map(commento => <Comment key={commento.id} {...commento} />)
            }
        </div>
    );
}
