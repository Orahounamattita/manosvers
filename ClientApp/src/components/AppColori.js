﻿import React, { Component } from 'react';
import { ColorList } from './ColorList';
import { AddColorForm } from './AddColorForm';
import { v4 } from 'uuid'


export class AppColori extends Component {
    static displayName = AppColori.name;

    constructor(props) {
        super(props)
        this.state = {
            colori: [
                {
                    "id": "8658c1d0-9eda-4a90-95e1-8001e8eb6036",
                    "titolo": "Ocean Blue",
                    "colore": "#0070ff",
                    "voto": 3

                },
                {
                    "id": "f9005b4e-975e-433d-a646-79df172e1dbb",
                    "titolo": "Tomato",
                    "colore": "#d10012",
                    "voto": 2
                },
                {
                    "id": "58d9caee-6ea6-4d7b-9984-65b145031979",
                    "titolo": "Lawn",
                    "colore": "#67bf4f",
                    "voto": 1
                },
                {
                    "id": "a5685c39-6bdc-4727-9188-6c9a00bf7f95",
                    "titolo": "Party Pink",
                    "colore": "#ff00f7",
                    "voto": 5
                }
            ]
        }
        this.rimuoviColore = this.rimuoviColore.bind(this);
        this.votaColore = this.votaColore.bind(this);
        this.aggiungiColore = this.aggiungiColore.bind(this);
    }

    votaColore(id, voto) {
        const colori = this.state.colori.map(colore => (colore.id !== id) ? colore : { ...colore, voto: voto })

        this.setState({ colori: colori }, function () { console.log(this.state.colori) })
        
    }

    rimuoviColore(id) {
        const colori = this.state.colori.filter(colore => (colore.id !== id))
        
        this.setState({ colori })
    }

    aggiungiColore(titolo, colore) {
        const colori = [...this.state.colori, {
            id: v4(),
            titolo,
            colore,
            voto: 0
        }]
        this.setState({ colori })
    }

    render() {
        const { votaColore, rimuoviColore } = this
        const { colori } = this.state
        return (
            <div>
                <AddColorForm aggiungiColore={this.aggiungiColore} />
                <ColorList colori={this.state.colori} votaColore={votaColore} rimuoviColore={rimuoviColore} />
            </div>
        );
    }
}