﻿import React, { Component } from 'react';
import './Stella.css';

const Stella = ({ selezionati = false, eventoClick = f => f }) =>
    <button className={selezionati ? "star selected" : "star"} onClick={eventoClick}></button >


export const StellaRating = ({ stelleSelezionate = 0, stelleTotali = 5, votaColore=f=>f }) =>
    <div className="star-rating">
        {[...Array(stelleTotali)].map((n, i) =>
            <Stella key={i} selezionati={i < stelleSelezionate} eventoClick={() => votaColore(i+1)} />
        )}<br />
        <p>{stelleSelezionate} of {stelleTotali} stars</p>
    </div>