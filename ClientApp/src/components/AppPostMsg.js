﻿import React, { Component } from 'react';
import { ListPosts } from './ListPosts';
import { AddPostForm } from './AddPostForm';
import { PostMsgProvider } from '../context/PostMsgContext';


export class AppPostMsg extends Component {
    constructor(props) {
        super(props)
    }
    //addPost(titolo) {
    //    const posts = [...this.state.posts, {
    //        id: v4(),
    //        titolo,
    //        like: 0,
    //        num_msg: 0,
    //        commenti: [{}]
    //    }]
    //    this.setState({ posts })
    //}

    //removePost(id) {
    //    const posts = this.state.posts.filter(post => (post.id !== id))
    //    this.setState({ posts })
    //}

    //likePost(id) {
    //    const posts = this.state.posts.map(post => (post.id !== id) ? post : { ...post, like: post.like + 1 } );
    //    this.setState({ posts: posts }, function () { console.log(this.state.posts) })
    //}

    //dislikePost(id) {
    //    const posts = this.state.posts.map(post => (post.id !== id) ? post : { ...post, like: post.like - 1 } );
    //    this.setState({ posts: posts }, function () { console.log(this.state.posts) })
    //}

    //answerPost(id, testo) {

    //    const posts = this.state.posts.map(post =>
    //        (post.id !== id) ?
    //            post : { ...post, commenti: [...post.commenti, { id: v4(), testo: testo }], num_msg: post.num_msg + 1 }
    //    )
    //    this.setState({ posts: posts }, function () { console.log(this.state.posts) })
    //}

    render() {
        return (
            <PostMsgProvider>
                <div className="cornice-app">
                    <AddPostForm />
                    <ListPosts />
                </div>
            </PostMsgProvider>
        );
    }
}