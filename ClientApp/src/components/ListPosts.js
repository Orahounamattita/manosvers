﻿import React, { Component, useState, useContext } from 'react';
import { AddCommentForm } from './AddCommentForm';
import { ListComment } from './ListComment';
import { PostMsgContext } from '../context/PostMsgContext';



const Post = ({ id, titolo, like, num_msg, commenti = [] }) => {
    const [posts, setPosts] = useContext(PostMsgContext);


    const removePost = e => {
        setPosts(posts.filter(post => (post.id !== id)))
    }

    const likePost = e => {
        setPosts(posts.map(post => (post.id !== id) ? post : { ...post, like: post.like + 1 }));
    }

    const dislikePost = e => {
        setPosts(posts.map(post => (post.id !== id) ? post : { ...post, like: post.like - 1 }));
    }
    return (
        <div className="col-md-6 form-group">
            <section>
                <div className="form-inline">
                    <h2>{titolo}</h2>
                    <button className="btn btn-red" onClick={removePost}><i className="fa fa-close" /></button>
                </div>
                <div className="form-inline">
                    <button className="btn " onClick={likePost}><i className="fa fa-rocket text-green" /></button>
                    <button className="btn " onClick={dislikePost}><i className="fa fa-fire text-red" /></button>
                    <p>{like} of likes</p>
                    <p>{num_msg} of commenti</p>
                </div>
                <div>
                    <AddCommentForm id={ id} />
                    <ListComment id={ id} />
                </div>
            </section>
        </div>
    );

}


export const ListPosts = () => {

    const [posts, setPosts] = useContext(PostMsgContext);
    return (
        <div className="elenco-post row">
            {(posts.length === 0) ?
                <p>Non ci sono Post al momento</p> :
                posts.map(post => <Post key={post.id} {...post} />)
            }
        </div>
    );

}

