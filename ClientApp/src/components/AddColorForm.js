﻿import React, { Component } from 'react';

export const AddColorForm = ({ aggiungiColore = f => f }) => {
    let _titolo, _colore
    const submit = e => {
        e.preventDefault()
        aggiungiColore(_titolo.value, _colore.value)
        _titolo.value = ''
        _colore.value = '#000000'
        _titolo.focus()
    }

    return(
        <form className="aggiungi-colore" onSubmit={submit}>
            <input ref={input => _titolo = input} type="text" placeholder="titolo colore..." required />
            <input ref={input => _colore = input} type="color" required />
            <button className="btn-success">ADD</button>
        </form>
    )
}
