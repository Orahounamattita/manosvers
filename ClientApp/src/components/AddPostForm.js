﻿import React, { Component, useState, useContext } from 'react';
import { PostMsgContext } from '../context/PostMsgContext';
import { v4 } from 'uuid';

export const AddPostForm = () => {
    let _titolo
    const [posts, setPosts] = useContext(PostMsgContext);


    const submit = e => {
        e.preventDefault()
        setPosts(prevPosts => [...prevPosts, { id: v4(), titolo: _titolo.value, like: 0, num_msg: 0, commenti: "" }])
        _titolo.value = ''
        _titolo.focus()
    }

    return (
        <form className="aggiungi-post" onSubmit={submit}>
            <input ref={input => _titolo = input} type="text" placeholder="inserisci il titolo del post" required />
            <button className="btn-success">ADD POST</button>
        </form>
    )
}