﻿import React, { Component, useState, useContext } from 'react';
import { PostMsgContext } from '../context/PostMsgContext';
import { v4 } from 'uuid';

export const AddCommentForm = ({ id}) => {
    let _testo
    const [posts, setPosts] = useContext(PostMsgContext);
    const postSelected = posts.filter(post => (post.id === id));

    const submit = e => {
        e.preventDefault();
        setPosts(posts.map(post =>
            (post.id !== id) ?
                post : { ...post, commenti: [...post.commenti, { id: v4(), testo: _testo.value }], num_msg: post.num_msg + 1 }))
        _testo.value = ''
        _testo.focus()
    }

    return (
        <form className="aggiungi-commento" onSubmit={submit}>
            <input ref={input => _testo = input} type="text" placeholder="inserisci qui il commento" required />
            <button className="btn-success">ADD COMMENT</button>
        </form>
    )
}
